import abc


class Menu(abc.ABC):
    """ abstract class that should detail what each menu class should have . Necessary as we need multiple menus
    going forward.
    """
    @abc.abstractmethod
    def print_menu(self, stdscr):
        pass

    @abc.abstractmethod
    def select_option(self, stdscr, current_row: int):
        pass

    @abc.abstractmethod
    def run(self, stdscr):
        pass

    