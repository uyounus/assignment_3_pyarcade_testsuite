from typing import Optional
import random
from input_system import MastermindInput
import curses
import time



class Mastermind:
    """ A class representing a Mastermind game session

        Args:
            width (int): The number of random digits to generate

            max_range (int): The range that a single digit can vary

    """
    master_game_data = list()
    attempts = 0
    generated_guess = list()

    def __init__(self, width: Optional[int] = 4, max_range: Optional[int] = 9):
        self.width = width
        self.max_range = max_range
        self.current_game_data = list()

#    @classmethod
    def generate_hidden_sequence(self) -> list():
        """
        Returns:
            hidden_sequence List[int]: A sequence of integers to be guessed

            by the player.
        """
        return list([random.randint(0, self.max_range) for _ in range(self.width)])


    def get_input_user(self, stdscr) -> str:
        """
            Gets a user input, calls the input handler class to synthesize the

            input and modify it for functional use in mastermind by making it a

            list. Also, it calls memory clear/reset if user prompts.
        """

        height, width = stdscr.getmaxyx()
        handler = MastermindInput()
        while True:
            stdscr.addstr(height//2, width//2, "Please Enter the 4 digit Number to be Guessed.\n Enter exit to Quit Game:" )
            stdscr.refresh()
#            user_input = str(input("Please Enter the 4 digit Number to be Guessed: "))
            user_input = stdscr.getstr(height//2 + 2, width//2).decode(encoding="utf-8")
            stdscr.addstr(height//2 + 2, width//2, "                   ")
            stdscr.refresh()
            
            # calling input_systems class to check input
            check = handler.handle_input(user_input, self)
            if check == True:
                # checking for the exit
                if user_input == 'exit':
                    return 'exit'
#                    break
                else:
                    # converting the string to a parsed string for comparison
                    return handler.convert_to_string(user_input)

    def evaluate_success(self, user_number: list, stdscr) -> bool:
        if user_number == self.generated_guess:
#            print("Great! You guessed the entire number correctly")
            self.current_game_data = list([(user_number[0], 'Correct Spot'), (user_number[1], 'Correct Spot'), (user_number[2], 'Correct Spot'), (user_number[3], 'Correct Spot')])
            Mastermind.master_game_data.append(self.current_game_data)
            
            self.display('True', stdscr)
            return True
        else:
            return self.evaluate_partial_success(user_number, stdscr)

    def evaluate_partial_success(self, user_number: list, stdscr) -> bool:
        Mastermind.attempts += 1
        for i in range(self.width):
            """
                Checking for equality of digits and storing in a
                local database and then adding to global.
                ...
                Also checking each element of user with each
                element of random number so that we can guess if it
                matched another string as well
            """
            counter = 0
            if user_number[i] == self.generated_guess[i]:
                self.current_game_data.append(list([user_number[i], 'Correct Spot']))
                counter += 1
            else:
                # multiple loops so that each element of user is
                # checked with each other element of random number
                for j in range(self.width):
                    if i != j:
                        if user_number[i] == self.generated_guess[j]:
                            self.current_game_data.append(list([user_number[i], 'Wrong Position']))
                            counter += 1
                    else:
                        continue
            # if the ith element if user hasn't matched with any
            # of the random number digits, it is guessed wrongly
            if counter == 0:
                self.current_game_data.append(list([user_number[i], 'Not in Sequence']))

        # Now adding the current game session to Mastermind
        Mastermind.master_game_data.append(self.current_game_data)

        self.display('False', stdscr)
        return False

    def display(self, win_state: str, stdscr) -> None:

        if win_state == 'True':
            # Adding blank space before printing on the same space to clear the line in curses windo
            stdscr.addstr(2, 2, "                                                                                        ")
            stdscr.refresh()
            stdscr.addstr(2, 2, "Great! You guessed the entire number correctly")

        elif win_state == 'False':
            # Adding blank space before printing on the same space to clear the line in curses window
            stdscr.addstr(2, 2, "                                                                                        ")
            stdscr.refresh()
            stdscr.addstr(2, 2, 'This guess is not completely correct. Check your performance after:' + str(Mastermind.attempts) + 'th try')
            stdscr.addstr(4, 2, str(self.current_game_data))

        stdscr.refresh()
        time.sleep(3)

#        curses.endwin()

    def evaluation(self):
        """
            This function runs based on the user input. It stops once the user
            has guessed the correct input, or in case of wrong guess, keeps
            asking user for next guesses.

            To keep it running even after correct guess(forever), "UNCOMMENT
            THE WHILE LOOP" below:
        """
        # while True:
        # Initializing curses screen over here
        stdscr = curses.initscr()
        stdscr.clear()

        Mastermind.attempts = 0
        self.current_game_data = list()
        Mastermind.generated_guess = self.generate_hidden_sequence()
       
        stdscr.addstr(0 ,1, "Generated Random Number" + str(Mastermind.generated_guess))
        stdscr.refresh()
       
#        print("Generated Random Number", Mastermind.generated_guess)
        while True:
            user_number = self.get_input_user(stdscr)
#                # Clearing the local data to show next attempt
#                self.current_game_data = list()
            if user_number == None or user_number == 'exit':
                stdscr.refresh()
                curses.endwin()
                break
            done = self.evaluate_success(user_number, stdscr)
            if done == True:
                # print("Attempts", self.attempts)
                stdscr.addstr(5, 5, "All Attempts" + str(self.attempts))
                stdscr.refresh()
                curses.endwin()
                break


#if __name__ == "__main__":
#    instance = Mastermind()
#    instance.evaluation()
#    print("Your overall history of the game is:", instance.master_game_data)
#    x = instance.get_input_user()
#    print(x)
