60/40 Joon/Usama
Joon worked on the main menu ui and subsequent game specific menus using curses as well as implementing the ui for his own game and integrating it with the game specific menu. Usama created the UI for his game as well as mastermind and merged the input functions together and integrating with his own game specific menus. Because developing the UI took learning a new library and debugging we agreed that Joon took on a bit more of the share of the work. Overall I don’t think we were prepared for the amount of work this project entailed. Both of us were interviewing at the time which took up a significant portion of our time. At the same time I believe we underestimated the time it would take to communicate, design a plan, delegate roles, integrate into each others code, and collaboratively work on different features.

Design pattern used:
We chose a factory method design pattern which we implemented in the main menu. The main menu acts as a point for the instantiation of different menu classes that is processed at run time. We had in mind as more games were added this would reduce the complexity of creating game specific menus as we had a fundamental interface that they could employ and extend. This way we have a set preset and know what to expect when encountering a new menu for a new game.

Found on file main.py line 16 
mainmenu = MainMenu.MainMenu(["Blackjack","Mastermind", "Minesweeper", "Exit"], 
                                    [MenuBlackjack.MenuBlackjack(), MenuMastermind.MenuMastermind(), MenuMinesweeper.MenuMinesweeper(), "exit"], "Main menu")

Also included is the menu.py interface that menus extend
class Menu(abc.ABC):
    """ abstract class that should detail what each menu class should have . Necessary as we need multiple menus
    going forward.
    """
    @abc.abstractmethod
    def print_menu(self, stdscr):
        pass

    @abc.abstractmethod
    def select_option(self, stdscr, current_row: int):
        pass

    @abc.abstractmethod
    def run(self, stdscr):
        pass



