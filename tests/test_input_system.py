from input_system import MastermindInput
from input_system import MinecraftInput
from input_system import BlackjackInput
from mastermind import Mastermind
import unittest


class InputSystemTestCase(unittest.TestCase):
    def testing_convert_to_string(self):
        handler = MastermindInput()
        string = str('1234')
        self.assertEqual(handler.convert_to_string(string), [1, 2, 3, 4])
        # Checking 2nd instance
        string_2 = str('14543')
        self.assertEqual(handler.convert_to_string(string_2), [1, 4, 5, 4, 3])

    def testing_handle_input_numeric(self):
        handler = MastermindInput()
        """
            This function in Input_System returns boolean True/False depending
            on if the argument string is numeric or 'clear/reset/exit command'.

            Otherwise it returns false
            """
        # testing the case where input is numeric
        string = str('1234')
        self.assertEqual(handler.handle_input(string), True)

    def testing_handle_input_alphabet(self):
        handler = MastermindInput()
        game = Mastermind()
        # testing the case where input is aplhabetic and equal to reset or
        # clear
        string_2 = str('clear')
        self.assertEqual(handler.handle_input(string_2, game), True)
        # testing case if input is neither numeric nor the commands required
        string_3 = str('faked123')
        self.assertEqual(handler.handle_input(string_3, game), False)

    def test_mastermind_data_clear_function(self):
        """
            Testing if the clear command by user clears the games history
            through the MEMORY CLEAR FUNCTION
        """
        game = Mastermind()
        handler = MastermindInput()
        Mastermind.master_game_data = [1, 2, 3, 4]
        clear_command = str('clear')
        handler.memory_clear(clear_command, game)
        self.assertEqual(game.master_game_data, list())

    def test_mastermind_attempts_clear_function(self):
        """
            Testing if the clear/reset command given by user clears the
            attempts history through the MEMORY CLEAR FUNCTION
        """
        game = Mastermind()
        handler = MastermindInput()
        Mastermind.attempts = 5

        #
        # Checking for the clear command by the user
        #

        clear_command = str('clear')
        handler.memory_clear(clear_command, game)
        self.assertEqual(game.attempts, 0)

        #
        # Also checking for the reset command by user
        #

        Mastermind.attempts = 10
        reset_command = str('reset')
        handler.memory_clear(reset_command, game)
        self.assertEqual(game.attempts, 0)
  
        """
        ======================================================================
            The test of Minecraft input start here
        ======================================================================
        """

    def test_minecraft_input_message(self):
        handler = MinecraftInput()
        # testing the minecraft inputs and comparing with the intended outputs
        helpmessage = ("test message")
        gridsize = 10

        # Setting the results of the input function
        # Testing if HELP is typed as input

        inputstring = str("help")
        result = handler.minecraft_input_handle(inputstring, gridsize, helpmessage + '\n')
        message = result['message']
        self.assertEqual(message, helpmessage + '\n')

    def test_minecraft_input_position(self):
        handler = MinecraftInput()
        helpmessage = ("test message")
        gridsize = 10
        inputstring = str("e3")

        # Testing if certain location is typed as input
        # Here the a5 is at (5,0) position with respective locations as (5,a)

        result = handler.minecraft_input_handle(inputstring, gridsize, helpmessage + '\n')
        position = result['cell']
        self.assertEqual(position, (2, 4))

    def test_minecraft_input_flag(self):
        handler = MinecraftInput()
        helpmessage = ("test message")
        gridsize = 10
        inputstring = str("e3")

        # Testing that the flag is returned if f is not typed in input

        result = handler.minecraft_input_handle(inputstring, gridsize, helpmessage + '\n')
        flag = result['flag']
        self.assertEqual(flag, False)

        """
        ======================================================================
            The test of Black Jack input start here
        ======================================================================
        """

    def blackjack_input_test_hit(self):
        binput = BlackjackInput()
        assert binput.handle_input("hit") == "hit"

    def blackjack_input_test_stand(self):
        binput = BlackjackInput()
        assert binput.handle_input("stand") == "stand"

    def blackjack_input_test_anything_else(self):
        binput = BlackjackInput()
        assert binput.handle_input("give me my money") == "invalid input, please hit or stand"
        assert binput.handle_input(67543) == "invalid input, please hit or stand"

    def blackjack_input_test_wrong_string(self):
        binput = BlackjackInput()
        assert binput.handle_input("call") == "invalid input, please hit or stand"


#if __name__ == "__main__":
#    test_suite = InputSystemTestCase()
#    test_suite.test_minecraft_input_flag()
