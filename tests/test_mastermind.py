from mastermind import Mastermind
import unittest


class MastermindTestCase(unittest.TestCase):
    def test_generate_random_sequence(self):
        game = Mastermind()
        self.assertEqual(len(game.generate_hidden_sequence()), 4)

#    def test_global_data_clear_function(self):
#        """
#            Testing if the clear command by user clears the games history
#            through the MEMORY CLEAR FUNCTION
#        """
#        game = Mastermind()
#        Mastermind.master_game_data = [1, 2, 3, 4]
#        clear_command = str('clear')
#        game.memory_clear(clear_command)
#        self.assertEqual(Mastermind.master_game_data, list())
#
#    def test_global_attempts_clear_function(self):
#        """
#            Testing if the clear/reset command given by user clears the
#            attempts history through the MEMORY CLEAR FUNCTION
#        """
#        game = Mastermind()
#        Mastermind.attempts = 5
#        #
#        # Checking for the clear command by the user
#        #
#        clear_command = str('clear')
#        game.memory_clear(clear_command)
#        self.assertEqual(game.attempts, 0)
#        #
#        # Also checking for the reset command by user
#        #
#        Mastermind.attempts = 10
#        reset_command = str('reset')
#        game.memory_clear(reset_command)
#        self.assertEqual(game.attempts, 0)

    def test_user_input_parsing(self):
        """
            Since this function takes in input from the user directly, and

            converts that to list, we have to enter the same list for

            comparsion which we input when prompted for input.
        """
        game = Mastermind()
        comparison_list = [1, 2, 3, 4]
        """
            When the command prompt asks for input, we put in 1234
        """
        user_guessed_list = game.get_input_user()
        self.assertEqual(user_guessed_list, comparison_list)

    def test_history_clearance_through_user(self):
        """
            In this, we will check if the clear/reset command obtained by

            user input function calls the Memory Clear Fucntion and deletes

            the games history.
        """
        Mastermind.master_game_data = [1, 2, 3, 4]
        Mastermind.attempts = 20
        Mastermind.current_game_data = list([5, 6, 7, 7])
        #
        # Creating instance of Mastermind to get user input
        #
        game = Mastermind()
        """
            When the command prompt asks for input, we put in clear/reset

            command. And then put in any 4 digit number '1234' because the

            function is designed to take user input for next round after

            clearing game history
        """
        game.get_input_user()
        # Check all global and current game memory
        self.assertEqual(game.master_game_data, list())
        self.assertEqual(game.attempts, 0)
        self.assertEqual(game.current_game_data, list())

    def test_evaluation_funtion_completely(self):
        """
            Because the evaluation function runs the entire game based on the
            user input, and it keeps running if the guesses are wrong, we can
            compare only certain instances of the function to check if game is
            proceeding correctly

            For this purpose, the original evaluation function prints the
            random generated number, so after guessing the same number,
            we can check visually if it is stored correctly in memory.

            Another check is to see if the memory gets cleared during the game,
            when clear is pressed, in order to check intergaration.

            Note:
                We could do more extensive testing, by inputting the correct
            guess (random number is printed each time for correct guessing) &
            tracing the keys pressed through python and pass that variable in
            assertEqual to be compared with game history - since game stores
            correct guess and exits immediately.
                But this testing is extensive and I believe is out of scope for
            this assignment.
        """
        game = Mastermind()
        game.evaluation()
        stored_memory = Mastermind.master_game_data
        #
        # Check visually through command window for stored data for below line
        #
        print('The memory stored in the game is:', stored_memory)
        #
        # Below Lines: Checking if memory clear works during the game.
        #
        # Method:
        # Guess a number wrong on 1st try and then enter clear on 2nd and enter
        # exit on 3rd prompt. This will clear memory and exit the game without
        # saving next attempt status
        #
        game.evaluation
        global_memory = Mastermind.master_game_data
        self.assertEqual(global_memory, list())


if __name__ == "__main__":
    check = MastermindTestCase()
    check.test_global_data_clear_function()
    check.test_global_attempts_clear_function()
#    check.test_user_input_parsing()
#    check.test_history_clearance_through_user()
    check.test_evaluation_funtion_completely()
