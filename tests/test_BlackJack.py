import unittest
from pyarcade.BlackJack import BlackJack
from pyarcade.BlackJack import Card


class TestBlackjack(unittest.TestCase):

    def test_setup(self):
        blackjack = BlackJack()
        assert len(blackjack.user_hand) == 2
        assert len(blackjack.house_hand) == 2
        blackjack.clear()

    def test_hit(self):
        blackjack = BlackJack()
        assert len(blackjack.user_hand) == 2
        blackjack.hit(blackjack.user_hand)
        assert len(blackjack.user_hand) == 3
        blackjack.clear()

    def test_clear(self):
        blackjack = BlackJack()
        assert len(blackjack.user_hand) == 2
        assert len(blackjack.house_hand) == 2
        blackjack.clear()
        assert len(blackjack.user_hand) == 0
        assert len(blackjack.house_hand) == 0

    def test_bust(self):
        blackjack = BlackJack()
        assert blackjack.bust() == "BUST"
        blackjack.clear()

    def test_draw(self):
        blackjack = BlackJack()
        assert blackjack.tie() == "TIE"
        blackjack.clear()

    def test_win(self):
        blackjack = BlackJack()
        assert blackjack.win() == "WIN BABY"
        blackjack.clear()

    def test_win_condition(self):
        blackjack = BlackJack()
        # draw
        assert blackjack.win_condition(22, 23) == "TIE"
        # bust
        assert blackjack.win_condition(24, 19) == "BUST"
        assert blackjack.win_condition(17, 19) == "BUST"
        # win
        assert blackjack.win_condition(1, 23) == "WIN BABY"
        assert blackjack.win_condition(20, 18) == "WIN BABY"
        blackjack.clear()

    def test_calculate_current_sum(self):
        blackjack = BlackJack()
        assert blackjack.calculate_current_sum([Card("Spades", 4), Card("Spades", 10)]) == 14
        assert blackjack.calculate_current_sum([Card("Spades", 11), Card("Spades", 10)]) == 20
        assert blackjack.calculate_current_sum([Card("Spades", 11), Card("Spades", 13)]) == 20
        assert blackjack.calculate_current_sum([Card("Spades", 11), Card("Spades", 14)]) == 21
        assert blackjack.calculate_current_sum([Card("Spades", 5), Card("Spades", 14)]) == 16
        blackjack.clear()

    def test_next_state_bust(self):
        blackjack = BlackJack()
        user_hand = [Card("Spades", 10), Card("Spades", 14)]
        house_hand = [Card("Spades", 11), Card("Spades", 14)]
        # bust when user hits on 21
        assert blackjack.next_state(user_hand, house_hand, "hit") == "BUST"
        blackjack.clear()

    def test_next_state_win(self):
        blackjack = BlackJack()
        user_hand = [Card("Spades", 10), Card("Spades", 14)]
        house_hand = [Card("Spades", 14), Card("Spades", 14)]
        # win when user stands on 21
        assert blackjack.next_state(user_hand, house_hand, "stand") == "WIN BABY"
        blackjack.clear()



